package person;
import java.util.Objects;

public class Person {
    private String name;
    private String surname;
    private String patronymic;
    private Integer age;
    private String sex;
    private String birthday;

    public Person(String name, String surname, String patronymic, int age, String sex, String birthday){
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.age = age;
        this.sex = sex;
        this.birthday = birthday;
    }

    public String getName(){
        return name;
    }
    public String getSurname(){
        return surname;
    }
    public String getPatronymic(){
        return patronymic;
    }
    public Integer getAge(){
        return age;
    }
    public String getSex(){
        return sex;
    }
    public String getBirthday(){
        return birthday;
    }

    public void setName(String name){
        this.name = name;
    }
    public void setSurname(String surname){
        this.surname = surname;
    }
    public void setPatronymic(String patronymic){
        this.patronymic = patronymic;
    }
    public void setAge(int age){
        this.age = age;
    }
    public void setSex(String sex){
        this.sex = sex;
    }
    public void setBirthday(String birthday){
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name) && surname.equals(person.surname) &&
                patronymic.equals(person.patronymic) && sex.equals(person.sex) &&
                birthday.equals(person.birthday) && age == person.age;
    }

    @Override
    public int hashCode(){
        return Objects.hash(name, surname, patronymic, sex, age, birthday);
    }

    @Override
    public String toString(){
        return "Person (" + name + " " + surname + " " + patronymic + " " + sex + " " + age + " " + birthday + ")";
    }
}
