import person.Person;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main_1 {
    public static void main(String[] args){
        Person person1 = new Person("Vitaliy", "Savushkin", "Petrovich", 20, "M", "14.09.1998");
        Person person2 = new Person("Mihail", "Kotovich", "Spanovich", 28, "M", "15.09.1990");
        Person person3 = new Person("Elizavita", "Grishina", "Makcimovna", 21, "F", "16.08.1997");
        Person person4 = new Person("Grigoriy", "Spivak", "Egorovich", 22, "M", "17.06.1996");
        Person person5 = new Person("Daniil", "Bolykov", "Igorevich", 18, "M", "18.09.2002");

        Person test = new Person("Ivan", "Vasilev", "Alexsandrovich", 40, "M", "24.07.1980");

        List<Person> person = new ArrayList<>();
        person.add(person1);
        person.add(person2);
        person.add(person3);
        person.add(person4);
        person.add(person5);
        person.add(person2);
        person.add(person2);
        person.add(person2);

        System.out.println(person);
// Task1
        boolean coincidence = person.contains(test);

        if(coincidence){
            System.out.println("Есть совпадение" + "\n");
        }else{
            System.out.println("Нет совпадения" + "\n");
        }
//-----------------------------------------------------------------------------
//Task2
        person.stream()
                .filter(persons -> (persons.getAge() <= 20) && (persons.getSurname().toLowerCase().matches("[abvgd].*")))
                .forEach(persons -> System.out.println("Человек с фамилией на а, б, в, г, д и возрастом меньше 20 лет: " + persons + "\n"));
//_____________________________________________________________________________
//Task3
        List<Person> all = new ArrayList<>();
        person.stream()
                .filter(per -> {if(all.contains(per)) return true; all.add(per); return false; })
                .sorted((Comparator.comparing(Person::getAge).reversed()))
                .forEach(p -> System.out.println("{" +  ":" + "{" + p + "}" + "}"));
//-----------------------------------------------------------------------------
    }
}
