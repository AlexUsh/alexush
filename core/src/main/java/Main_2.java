import java.util.Scanner;

public class Main_2{
    // Перевод долларов в рубли
    public double transferRu(double usd){
        return Math.floor(usd * 76.40);
    }

    // Перевод евро в рубли
    public  double transferRub(double eur){
        return Math.floor(eur * 90.36);
    }

    public static void main(String[] args) {
        Main_2 forex = new Main_2();

        System.out.print("Введите сумму долларов и евро через пробел (Пример: 34,56 45,67): ");
        Scanner scan = new Scanner(System.in);
        double usd = scan.nextDouble();
        double eur = scan.nextDouble();

        System.out.println("Доллар: " + usd + " <--Перевод--> " + forex.transferRu(usd) + " рублей (курс на 10.11.2020)");
        System.out.println("Евро: " + eur + " <--Перевод--> " + forex.transferRub(eur) + " рублей (курс на 10.11.2020)");
    }
}
